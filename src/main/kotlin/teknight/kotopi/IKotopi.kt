package teknight.kotopi

import teknight.kotopi.spec.Status
import teknight.kotopi.spec.StatusCode
import kotlin.reflect.KClass
import kotlin.reflect.KFunction

interface IKotopi {
    fun add(function: KFunction<*>, canReturn: List<StatusCode>)
    fun add(function: KFunction<*>, vararg canReturn: Status) = add(function, canReturn.map { it.code })
    fun add(controller: KClass<*>)
}