package teknight.kotopi.gen

import teknight.kotopi.spec.OpenApiSpec

interface ICodeGenerator {
    fun generate(spec: OpenApiSpec): String
}