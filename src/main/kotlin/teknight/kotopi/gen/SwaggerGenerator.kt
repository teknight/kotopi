package teknight.kotopi.gen

import teknight.kotopi.spec.*

const val PATHS = "paths"
const val SUMMARY = "summary"
const val PARAMETERS = "parameters"
const val RESPONSES = "responses"
const val NAME = "name"
const val IN = "in"
const val DESCRIPTION = "description"
const val REQUIRED = "required"
const val SCHEMA = "schema"
const val TYPE = "type"
const val ITEMS = "items"
const val ARRAY = "array"
const val REF = "\$ref"

class SwaggerGenerator(val kotopiLink: Boolean = true, val swaggerLink: Boolean = false): ICodeGenerator {
    override fun generate(spec: OpenApiSpec): String {
        return genPaths(spec.functions).joinToString()
    }

    private fun genPaths(paths: Map<String, List<FunctionSpec>>): List<String> {
        return "$PATHS:" / paths.flatMap { (key, value) ->
            "/$key:" / value.flatMap { genRequest(it) }
        }
    }

    private fun genRequest(request: FunctionSpec): List<String> {
        return "${request.type}:" / genRequestBody(request)
    }

    private fun genRequestBody(request: FunctionSpec): List<String> {
        return listOf(
            "$SUMMARY: ${request.summary}"
        ) + genParameters(request.parameters) + genResponses(request.responses)
    }

    private fun genResponses(responses: List<Response>): List<String> {
        return "$RESPONSES:" / responses.flatMap { genResponse(it) }
    }

    private fun genResponse(response: Response): List<String> {
        return "${response.code}" / (listOf(
            "$DESCRIPTION: ${response.description}"
        ) + genType(response.type))
    }

    private fun genParameters(parameters: List<Parameter>): List<String> {
        return listOf("$PARAMETERS:") + parameters.flatMap {
            "- $NAME: ${it.name}" / (listOf(
                "$IN: ${it.from}",
                "$DESCRIPTION: ${it.description}",
                "$REQUIRED: ${it.required}"
            ) + genType(it.type))
        }
    }

    private fun genType(type: OpenApiType, advancedPrefix: String = SCHEMA): List<String> {
        return when (type) {
            is JsonType.Object,
            is JsonType.Array,
            is JsonType.ObjectReference -> "$advancedPrefix: " / genAdvancedType(type)
            JsonType.String -> listOf("$TYPE: \"string\"")
            JsonType.Number -> listOf("$TYPE: \"number\"")
            JsonType.Boolean -> listOf("$TYPE: \"boolean\"")
            OpenApiType.Unit -> listOf("")
        }
    }

    private fun genAdvancedType(type: OpenApiType): List<String> {
        return when (type) {
            is JsonType.Object -> TODO()
            is JsonType.Array -> "$REF: " / listOf("$TYPE: \"$ARRAY\"") + genType(type.type, ITEMS)
            is JsonType.ObjectReference -> listOf("$REF: \"${type.typeName}\"")
            else -> throw Exception("Simple type got into advanced type generation!")
        }
    }

    private operator fun String.div(other: List<String>) = listOf(this) + indented(other)
}