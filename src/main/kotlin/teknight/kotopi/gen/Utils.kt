package teknight.kotopi.gen


fun indented(strings: List<String>) = strings.map { "  $it" }