package teknight.kotopi.spec

inline class StatusCode(val value: Int)
enum class Status(val code: StatusCode) {
    Ok(StatusCode(200)),
    Created(StatusCode(201))
}