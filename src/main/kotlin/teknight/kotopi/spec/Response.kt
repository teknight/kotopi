package teknight.kotopi.spec

data class Response(val code: Int, val type: OpenApiType, val description: String)