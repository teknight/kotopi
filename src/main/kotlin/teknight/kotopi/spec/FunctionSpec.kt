package teknight.kotopi.spec

data class FunctionSpec(
    val type: RequestType,
    val summary: String,
    val parameters: List<Parameter>,
    val responses: List<Response>
)