package teknight.kotopi.spec


data class OpenApiSpec(val functions: Map<String, List<FunctionSpec>>)