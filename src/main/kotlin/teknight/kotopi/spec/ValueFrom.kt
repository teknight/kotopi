package teknight.kotopi.spec

enum class ValueFrom {
    Body,
    Path
}