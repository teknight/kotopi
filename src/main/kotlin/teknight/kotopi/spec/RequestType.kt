package teknight.kotopi.spec

enum class RequestType {
    GET,
    POST,
    PUT,
    DELETE
}