package teknight.kotopi.spec

sealed class OpenApiType {
    object Unit: OpenApiType()
}
sealed class JsonType: OpenApiType() {
    data class Object(val values: Map<kotlin.String, JsonType>) : JsonType()
    data class Array(val type: JsonType) : JsonType()
    data class ObjectReference(val typeName: kotlin.String) : JsonType()
    object String : JsonType()
    object Number : JsonType()
    object Boolean : JsonType()
}
