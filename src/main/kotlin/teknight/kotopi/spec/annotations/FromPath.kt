package teknight.kotopi.spec.annotations

@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class FromPath