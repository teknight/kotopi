package teknight.kotopi.spec

data class Parameter(
    val name: String,
    val type: OpenApiType,
    val from: ValueFrom,
    val description: String = "",
    val required: Boolean = true
)