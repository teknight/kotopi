package teknight.kotopi.parser

import teknight.kotopi.spec.OpenApiType
import kotlin.reflect.KType

interface IClassTypeParser {
    fun parseClassType(type: KType, expandClass: Boolean = true): OpenApiType
}