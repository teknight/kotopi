package teknight.kotopi.parser

import teknight.kotopi.spec.OpenApiType

data class FunctionType(val parameters: Map<String, OpenApiType>, val returnType: OpenApiType)