package teknight.kotopi.parser

import kotlin.reflect.KFunction

interface IFunctionTypeParser {
    fun parseFunctionType(func: KFunction<*>): FunctionType
}