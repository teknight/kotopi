package teknight.kotopi.parser

import teknight.kotopi.spec.ValueFrom
import teknight.kotopi.spec.annotations.FromBody
import teknight.kotopi.spec.annotations.FromPath
import kotlin.reflect.KFunction
import kotlin.reflect.full.findAnnotation

class AnnotationParser {
    fun parseFunctionAnnotations(function: KFunction<*>) {
        function.annotations.map {

        }
    }

    fun parseParameterAnnotations(function: KFunction<*>) = function.parameters.map {
        ParameterAnnotation(
            when {
                it.findAnnotation<FromBody>() != null -> AnnotationValue(ValueFrom.Body)
                it.findAnnotation<FromPath>() != null -> AnnotationValue(ValueFrom.Path)
                else -> AnnotationValue(false)
            }
        )
    }
}