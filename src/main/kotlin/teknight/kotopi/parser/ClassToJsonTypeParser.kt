package teknight.kotopi.parser

import teknight.kotopi.spec.*
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.isSubclassOf

class ClassToJsonTypeParser: IClassTypeParser {
    override fun parseClassType(type: KType, expandClass: Boolean) =
        parseClassType(type, if (expandClass) ObjectReference.No else ObjectReference.Next)

    private fun parseClassType(type: KType, objectReference: ObjectReference): JsonType =
        when(val clazz = (type.classifier as KClass<*>)) {
            Int::class -> JsonType.Number
            Double::class -> JsonType.Number
            String::class -> JsonType.String
            Char::class -> JsonType.String
            Boolean::class -> JsonType.Boolean
            else -> {
                if (clazz.isSubclassOf(Collection::class)) {
                    parseCollectionType(type, objectReference)
                } else {
                    parseClassType(clazz, objectReference)
                }
            }
        }

    private fun parseCollectionType(type: KType, objectReference: ObjectReference): JsonType.Array {
        val listType = type.arguments.first().type
        return JsonType.Array(parseClassType(listType!!, objectReference))
    }

    private fun parseClassType(clazz: KClass<*>, objectReference: ObjectReference): JsonType {
        return if (objectReference == ObjectReference.Now && clazz.qualifiedName != null) {
            JsonType.ObjectReference(clazz.qualifiedName!!)
        } else {
            JsonType.Object(
                clazz.declaredMemberProperties.map {
                    it.name to parseClassType(
                        it.returnType,
                        if (objectReference == ObjectReference.Next) ObjectReference.Now else objectReference
                    )
                }.toMap()
            )
        }
    }

    private enum class ObjectReference {
        No,
        Next,
        Now
    }
}