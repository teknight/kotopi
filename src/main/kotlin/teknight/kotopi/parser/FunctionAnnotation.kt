package teknight.kotopi.parser

import teknight.kotopi.spec.RequestType
import teknight.kotopi.spec.ValueFrom

data class FunctionAnnotation(val requestType: AnnotationValue<RequestType>)
data class ParameterAnnotation(val valueFrom: AnnotationValue<ValueFrom>)

class AnnotationValue<T>(var isSet: Boolean = false, value: T? = null) {
    var value = value
        set(value) {
            isSet = true
            field = value
        }

    constructor(value: T): this(true, value)

    override fun equals(other: Any?): Boolean {
        return if (other !is AnnotationValue<*>) {
            false
        } else other.isSet == isSet && (!isSet || value == other.value)
    }

    override fun hashCode(): Int {
        var result = isSet.hashCode()
        result = 31 * result + (value?.hashCode() ?: 0)
        return result
    }
}