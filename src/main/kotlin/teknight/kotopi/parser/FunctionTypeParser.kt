package teknight.kotopi.parser

import teknight.kotopi.spec.OpenApiType
import kotlin.reflect.KFunction
import kotlin.reflect.full.starProjectedType

class FunctionTypeParser(private val classTypeParser: IClassTypeParser): IFunctionTypeParser {
    override fun parseFunctionType(func: KFunction<*>) =
        FunctionType(
            func.parameters.map { it.name!! to classTypeParser.parseClassType(it.type) }.toMap(),
            func.returnType.let {
                if (it == Unit::class.starProjectedType) OpenApiType.Unit else classTypeParser.parseClassType(it)
            }
        )
}