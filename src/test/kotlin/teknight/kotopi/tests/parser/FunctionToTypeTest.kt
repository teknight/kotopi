package teknight.kotopi.tests.parser

import org.amshove.kluent.shouldEqual
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature
import teknight.kotopi.parser.ClassToJsonTypeParser
import teknight.kotopi.parser.FunctionTypeParser
import teknight.kotopi.spec.JsonType
import teknight.kotopi.spec.OpenApiType

data class SimpleType(val i1: Int)
data class AdvancedType(val l1: List<List<SimpleType>>)

fun testFunc1() {}
fun testFunc2(i1: Int) {}
fun testFunc3(l1: List<String>) {}
fun testFunc4() = emptyList<Int>()
fun testFunc5(at1: AdvancedType, at2: AdvancedType) = at1

object FunctionToTypeTest: Spek({
    Feature("Function Type parser") {
        with(FunctionTypeParser(ClassToJsonTypeParser())) {
            Scenario("Parse a kotlin function") {
                listOf(
                    ::testFunc1 shouldReturn OpenApiType.Unit whenGiven NoParameters,
                    ::testFunc2 shouldReturn OpenApiType.Unit whenGiven ("i1" withType JsonType.Number),
                    ::testFunc3 shouldReturn OpenApiType.Unit whenGiven ("l1" withType JsonArrayString),
                    ::testFunc4 shouldReturn JsonArrayNumber,
                    ::testFunc5 shouldReturn jsonObjectOf(
                        "l1" withType jsonArrayOf(jsonArrayOf(jsonObjectOf("i1" withType JsonType.Number)))
                    ) whenGiven listOf(
                        "at1" withType jsonObjectOf(
                            "l1" withType jsonArrayOf(jsonArrayOf(jsonObjectOf("i1" withType JsonType.Number)))
                        ),
                        "at2" withType jsonObjectOf(
                            "l1" withType jsonArrayOf(jsonArrayOf(jsonObjectOf("i1" withType JsonType.Number)))
                        )
                    )
                ).forEach {
                    Then("Given ${it.first.name} it should return ${it.second}") {
                        it.second shouldEqual parseFunctionType(it.first)
                    }
                }
            }
        }
    }
})