package teknight.kotopi.tests.parser

import teknight.kotopi.parser.FunctionType
import teknight.kotopi.spec.*
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KType
import kotlin.reflect.KTypeProjection
import kotlin.reflect.full.createType
import kotlin.reflect.full.starProjectedType

infix fun KFunction<*>.shouldReturn(returnType: OpenApiType) = Pair(this,
    FunctionType(emptyMap(), returnType)
)
infix fun Pair<KFunction<*>, FunctionType>.whenGiven(params: List<Pair<String, OpenApiType>>) =
    Pair(first, second.copy(params.toMap() + second.parameters))
infix fun Pair<KFunction<*>, FunctionType>.whenGiven(param: Pair<String, OpenApiType>) = whenGiven(listOf(param))
infix fun String.withType(type: OpenApiType) = to(type)

inline fun<reified T> listTypeOf() = listTypeOf(T::class)
fun listTypeOf(clazz: KClass<*>) = listTypeOf(clazz.starProjectedType)
fun listTypeOf(type: KType) = List::class.createType(listOf(KTypeProjection.invariant(type)))


val NoParameters = emptyList<Pair<String, OpenApiType>>()
