package teknight.kotopi.tests.parser

import teknight.kotopi.parser.AnnotationValue
import teknight.kotopi.parser.ParameterAnnotation
import teknight.kotopi.spec.ValueFrom

fun paramAnnotationsOf(vararg valueFrom: ValueFrom?) = valueFrom.map {
    ParameterAnnotation(it?.let { value -> AnnotationValue(value) } ?: AnnotationValue(false))
}