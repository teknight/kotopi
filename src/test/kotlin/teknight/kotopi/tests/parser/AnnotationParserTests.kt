package teknight.kotopi.tests.parser

import org.amshove.kluent.shouldEqual
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature
import teknight.kotopi.parser.AnnotationParser
import teknight.kotopi.spec.ValueFrom
import teknight.kotopi.spec.annotations.FromBody
import teknight.kotopi.spec.annotations.FromPath

fun annotationFunc1() {}
fun annotationFunc2(s1: String) {}
fun annotationFunc3(@FromBody s1: String) {}
fun annotationFunc4(@FromBody s1: String, @FromBody s2: String) {}
fun annotationFunc5(s1: String, @FromBody s2: String) {}
fun annotationFunc6(@FromBody s1: String, s2: String) {}
fun annotationFunc7(@FromBody s1: String, @FromBody s2: String, @FromBody s3: String) = "Hello"
fun annotationFunc8(@FromBody s1: String, @FromPath s2: String) {}
fun annotationFunc9(@FromPath s1: String, @FromBody s2: String) = 5

object AnnotationParserTests: Spek({
    Feature("Parameter annotation parser tests") {
        val annotationParser = AnnotationParser()
        Scenario("Parse annotations from parameters") {
            listOf(
                ::annotationFunc1 to emptyList(),
                ::annotationFunc2 to paramAnnotationsOf(null),
                ::annotationFunc3 to paramAnnotationsOf(ValueFrom.Body),
                ::annotationFunc4 to paramAnnotationsOf(ValueFrom.Body, ValueFrom.Body),
                ::annotationFunc5 to paramAnnotationsOf(null, ValueFrom.Body),
                ::annotationFunc6 to paramAnnotationsOf(ValueFrom.Body, null),
                ::annotationFunc7 to paramAnnotationsOf(ValueFrom.Body, ValueFrom.Body, ValueFrom.Body),
                ::annotationFunc8 to paramAnnotationsOf(ValueFrom.Body, ValueFrom.Path),
                ::annotationFunc9 to paramAnnotationsOf(ValueFrom.Path, ValueFrom.Body)
            ).forEach {
                Then("Given ${it.first.name} it should return ${it.second}") {
                    annotationParser.parseParameterAnnotations(it.first) shouldEqual it.second
                }
            }
        }
    }
})