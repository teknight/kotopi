package teknight.kotopi.tests.parser

import org.amshove.kluent.shouldEqual
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature
import teknight.kotopi.parser.ClassToJsonTypeParser
import teknight.kotopi.spec.JsonType
import kotlin.reflect.full.starProjectedType

data class TestOne(val i1: Int)
data class TestTwo(val l1: List<TestOne>)
data class TestThree(val l1: List<List<String>>)

object ClassToJsonTypeTest: Spek({
    Feature("Class Type parser expand classes") {
        with(ClassToJsonTypeParser()) {
            Scenario("Parse a kotlin type to a parser type") {
                listOf(
                    Int::class to JsonType.Number,
                    Double::class to JsonType.Number,
                    Boolean::class to JsonType.Boolean,
                    String::class to JsonType.String,
                    Char::class to JsonType.String
                ).forEach {
                    val type = it.first.starProjectedType
                    Then("Type $type should equal ${it.second}") {
                        parseClassType(type) shouldEqual it.second
                    }
                }
            }
            Scenario("Parse a kotlin collection type to parser type") {
                listOf(
                    listTypeOf<String>() to JsonArrayString,
                    listTypeOf(listTypeOf<String>()) to JsonType.Array(JsonArrayString)
                ).forEach {
                    Then("Type ${it.first} should equal ${it.second}") {
                        parseClassType(it.first) shouldEqual it.second
                    }
                }
            }
            Scenario("Parse a kotlin class type to parser type") {
                listOf(
                    TestOne::class to jsonObjectOf("i1" withType JsonType.Number),
                    TestTwo::class to jsonObjectOf(
                        "l1" withType jsonArrayOf(jsonObjectOf("i1" withType JsonType.Number))
                    ),
                    TestThree::class to jsonObjectOf("l1" withType JsonType.Array(JsonArrayString))
                ).forEach {
                    val type = it.first.starProjectedType
                    Then("Type $type should equal ${it.second}") {
                        parseClassType(type) shouldEqual it.second
                    }
                }
            }
        }
    }

    Feature("Class Type parser no expand classes") {
        with(ClassToJsonTypeParser()) {
            Scenario("Parse a kotlin type to a parser type") {
                listOf(
                    Int::class to JsonType.Number,
                    Double::class to JsonType.Number,
                    Boolean::class to JsonType.Boolean,
                    String::class to JsonType.String,
                    Char::class to JsonType.String
                ).forEach {
                    val type = it.first.starProjectedType
                    Then("Type $type should equal ${it.second}") {
                        parseClassType(type, false) shouldEqual it.second
                    }
                }
            }
            Scenario("Parse a kotlin collection type to parser type") {
                listOf(
                    listTypeOf<String>() to JsonArrayString,
                    listTypeOf(listTypeOf<String>()) to JsonType.Array(JsonArrayString)
                ).forEach {
                    Then("Type ${it.first} should equal ${it.second}") {
                        parseClassType(it.first, false) shouldEqual it.second
                    }
                }
            }
            Scenario("Parse a kotlin class type to parser type") {
                listOf(
                    TestOne::class to jsonObjectOf("i1" withType JsonType.Number),
                    TestTwo::class to jsonObjectOf(
                        "l1" withType jsonArrayOf(JsonType.ObjectReference(TestOne::class.qualifiedName!!))
                    ),
                    TestThree::class to jsonObjectOf("l1" withType JsonType.Array(JsonArrayString))
                ).forEach {
                    val type = it.first.starProjectedType
                    Then("Type $type should equal ${it.second}") {
                        parseClassType(type, false) shouldEqual it.second
                    }
                }
            }
        }
    }
})