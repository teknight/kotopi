package teknight.kotopi.tests.parser

import teknight.kotopi.spec.JsonType

infix fun String.withType(type: JsonType) = to(type)

fun jsonObjectOf(vararg attributes: Pair<String, JsonType>) = JsonType.Object(attributes.toMap())
fun jsonArrayOf(type: JsonType) = JsonType.Array(type)
val JsonArrayString = JsonType.Array(JsonType.String)
val JsonArrayNumber = JsonType.Array(JsonType.Number)