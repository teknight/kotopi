package teknight.kotopi.tests.parser

import org.amshove.kluent.shouldEqual
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature
import teknight.kotopi.parser.AnnotationValue

object AnnotationValueTest: Spek({
    Feature("Annotation value") {
        Scenario("Adding value in constructor") {
            Then("given 5 it should have isSet to true and the value 5 in value var") {
                val annotationValue = AnnotationValue(5)
                annotationValue.isSet shouldEqual true
                annotationValue.value shouldEqual 5
            }
            Then("given \"Kotopi\" it should have isSet to true and the value 5 in value var") {
                val annotationValue = AnnotationValue("Kotopi")
                annotationValue.isSet shouldEqual true
                annotationValue.value shouldEqual "Kotopi"
            }
        }
        Scenario("Not adding value in constructor") {
            Then("It should have isSet to false and value var should be null") {
                val annotationValue = AnnotationValue<Unit>()
                annotationValue.isSet shouldEqual false
                annotationValue.value shouldEqual null
            }
        }
        Scenario("Not adding value in constructor, but setting value later") {
            Then("It should have isSet to true and value var should be the value set later") {
                val annotationValue = AnnotationValue<String>()
                annotationValue.value = "Kotopi"
                annotationValue.isSet shouldEqual true
                annotationValue.value shouldEqual "Kotopi"
            }
        }
    }
})